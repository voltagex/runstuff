﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NullReference.Win32;
using System.ComponentModel;
namespace DoYouEvenElevateBro
{
    class Program
    {
        static void Main(string[] args)
        {
            var port = string.Empty;
            if (args[0] == "/port" && args.Length == 2)
            {
                port = args[1];
                try
                {
                    HttpApi.ReserveUrl("http://*:" + port + "/");
                }

                catch (Win32Exception)
                {
                    Environment.Exit(1);
                }
            }

            Environment.Exit(0);
        }
    }
}
