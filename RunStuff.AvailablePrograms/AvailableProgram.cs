﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunStuff.AvailablePrograms
{
    public class AvailableProgram
    {
        public virtual string Path { get; protected set; }
        public virtual string Name { get; set; }
    }
}
