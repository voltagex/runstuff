﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunStuff.AvailablePrograms
{
    public class TeamViewer : AvailableProgram
    {
        private string Find()
        {
            var programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            var teamviewer = programFiles + @"\TeamViewer\Version8\TeamViewer.exe";
            if (File.Exists(teamviewer))
            {
                return teamviewer;
            }
            return "";
        }

        private string path = null;

        public override string Path
        {
            get
            {
                if (path == null)
                {
                    path = this.Find();
                }
                return path;
            }
            protected set {path = value;}
        }

        public TeamViewer() { Name = "TeamViewer"; }

        
    }
}
