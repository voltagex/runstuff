﻿using ServiceStack.ServiceHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;
namespace RunStuff.DTOs
{
    [Route("/Programs/{Name}")]
    [Route("/Programs/")]
    public class AvailableProgram
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string ResponseStatus { get; set; }
        public bool Kill { get; set; }
    }
}
