﻿using ServiceStack.ServiceInterface.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunStuff.DTOs
{
    public class AvailableProgramResponse
    {

        public string Path { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public ResponseStatus ResponseStatus { get; set; }

    }
}
