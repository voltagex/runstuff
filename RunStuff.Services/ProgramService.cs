﻿using Funq;
using RunStuff.DTOs;
using ServiceStack.OrmLite;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.WebHost.Endpoints;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunStuff.Services
{
    public class ProgramService : Service
    {
        Container container = null;
        public ProgramService()
        {
            container = EndpointHost.Config.ServiceManager.Container;
        }

        public AvailableProgramResponse Post(AvailableProgram program)
        {
            var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection();
            var programs = db.Where<AvailableProgram>(new { Name = program.Name }).FirstOrDefault();
            if (programs != null)
            {
                Process.Start(programs.Path);
            }
            else
            {
                throw new ProgramNotFoundException();
            }
            return new AvailableProgramResponse() { Name = program.Name, Status = "Starting" };
        }

        public AvailableProgramResponse Delete(AvailableProgram program)
        {
            var targetprocess = Process.GetProcessesByName(program.Name).FirstOrDefault();
            if (targetprocess == null)
                throw new ProgramNotFoundException();
            var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection();
            var programs = db.Where<AvailableProgram>(new { Name = program.Name }).FirstOrDefault();
            if (programs.Name != null)
            {                
                var result = targetprocess.CloseMainWindow();
                if (program.Kill)
                {
                    targetprocess.Kill();
                }
            }

            return new AvailableProgramResponse() {Name = programs.Name, Status = targetprocess.HasExited ? "Exited" : "Running"};
        }

        public List<AvailableProgramResponse> Options(AvailableProgram program)
        {
            var processes = from process in Process.GetProcesses()
                            select new AvailableProgramResponse { Name = process.ProcessName, Path = process.StartInfo.WorkingDirectory, Status = "Running" };
            return processes.ToList();
        }


    }
}
