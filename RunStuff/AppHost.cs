﻿using Funq;
using System.Threading;
using ServiceStack.DataAnnotations;
using ServiceStack.ServiceHost;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;
using System.Net;
using RunStuff.Services;
using ServiceStack.Logging;
using ServiceStack.Logging.Support.Logging;
using System.Reflection;
using System.Linq;
using System;
using RunStuff.AvailablePrograms;
using ServiceStack.OrmLite;
using RunStuff.DTOs;

namespace RunStuff
{


    public class AppHost : AppHostHttpListenerBase
    {
        public AppHost() : base("Test", typeof(ProgramService).Assembly) { }

        public override void Configure(Container container)
        {
            LogManager.LogFactory = new ConsoleLogFactory();
            var instances = from t in typeof(AvailablePrograms.AvailableProgram).Assembly.GetTypes()
                            where t.IsInstanceOf(typeof(AvailablePrograms.AvailableProgram)) && !t.IsAbstract
                            select Activator.CreateInstance(t) as AvailablePrograms.AvailableProgram;

            var dtos = from instance in instances
                       where (!string.IsNullOrEmpty(instance.Path))
                       select new DTOs.AvailableProgram { Path = instance.Path, Name = instance.Name };

            container.Register<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider));

            using (var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection())
            {
                db.CreateTableIfNotExists<DTOs.AvailableProgram>();
                db.InsertAll(dtos);
            }
    
            SetConfig(new EndpointHostConfig {
                MapExceptionToStatusCode = {{typeof(ProgramNotFoundException),404}}}
                );
        }


    }
}

