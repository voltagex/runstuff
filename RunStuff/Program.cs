﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ServiceStack.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
namespace RunStuff
{
    class Program
    {
        static void Main(string[] args)
        {
            var appHost = new AppHost();
            string port = "2001";
            appHost.Init();
            
            try
            {
                appHost.Start("http://*:"+port+"/");
                "\n\nListening on http://*:2001/..".Print();
                "Type Ctrl+C to quit..".Print();
                Thread.Sleep(Timeout.Infinite);
            }

            catch (HttpListenerException)
            {
                "Unable to bind to a port - you're going to have to let me elevate to admin to fix this (Ctrl+C to quit, enter to continue)".Print();
                Console.ReadLine();

                var elevatedProcess = Process.Start("DoYouEvenElevateBro.exe", "/port " + port);
                "Waiting for the elevated process to finish".Print();
                elevatedProcess.WaitForExit();
                
                int retVal = elevatedProcess.ExitCode;
                if (retVal == 0)
                {
                    "Restart the program and you should be able to bind to that port as non-admin".Print();
                }

                else
                {
                    "Could not bind to the port, maybe it was already in use?".Print();
                }

                "Enter to continue...".Print();
                Console.ReadLine();
            }
        }



    }
}

